import { IsNotEmpty, MinLength,IsPositive, Matches } from 'class-validator';
export class CreateProductDto {

  @IsNotEmpty()
  @MinLength(8)
  name:string

  @IsNotEmpty()
  @IsPositive()
  price:number




}
